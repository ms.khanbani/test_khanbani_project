import 'package:flutter/material.dart';
import 'package:test_project_khanbani/services/consts.dart';
import 'package:test_project_khanbani/tab_setting.dart';
import 'package:test_project_khanbani/tab_transactions.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  int currentTab = 0;
  final List<Widget> screens = [TabSetting(), TransactionPage()];

  Widget currentScreen = TabSetting();
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:  PageStorage(bucket: bucket, child: currentScreen),
      floatingActionButton: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Colors.white,
          ),
          padding: EdgeInsets.all(0),
          width: 72,
          height: 72,
          child: FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0),
                  side: BorderSide(color: Colors.transparent)),
              padding: EdgeInsets.all(0),
              child: Image.asset(
                'assets/images/Group 3803.png',
                fit: BoxFit.cover,
              ),
              //elevation: 0,
              //backgroundColor: Colors.white,
              onPressed: () {})),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAlias,
        shape: CircularNotchedRectangle(),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen = TabSetting();
                        currentTab = 0;
                      });
                    },
                    child: SizedBox(
                        width: 35,
                        height: 60,
                        child: Image.asset('assets/images/Group 3804.png')),
                    minWidth: 40,
                  ),
                  SizedBox(
                    width: 120,
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen = TabSetting();
                        currentTab = 0;
                      });
                    },
                    child: SizedBox(
                        width: 35,
                        height: 60,
                        child: Image.asset('assets/images/Group 3805.png')),
                    minWidth: 40,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
