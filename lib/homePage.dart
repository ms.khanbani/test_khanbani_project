import 'package:flutter/material.dart';
import 'package:test_project_khanbani/services/consts.dart';
import 'package:test_project_khanbani/dashboard.dart';


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
 
    Widget _lblTitle = Center(
        child: Text(
      'ورود به حساب کاربری',
      style: TextStyle(
          fontFamily: 'IranSans', fontSize: 19, color: AppDefaults.labelColor),
    ));

    Widget _avatar = Container(
      width: 110,
      height: 110,
      child: Image(image: AssetImage('assets/images/Group 2855.png')),
    );

    Widget _takePhoto = Positioned(
        bottom: -20,
        left: 2,
        child: SizedBox(
            width: 110,
            height: 32,
            child: FlatButton(
                padding: EdgeInsets.all(0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.transparent)),
                highlightColor: Colors.white,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'انتخاب عکس',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'IranSans',
                            fontSize: 12),
                      ),
                      Icon(
                        Icons.photo_camera,
                        color: Colors.white,
                        size: 17,
                      )
                    ]),
                color: AppDefaults.buttonBgColor,
                onPressed: () {})));

    Widget _lblUserName = Align(
        alignment: Alignment.centerRight,
        child: Text('نام کاربری',
            style: TextStyle(
                fontFamily: 'IranSans',
                color: Color.fromARGB(255, 166, 168, 195))));

    Widget _lblPassword = Align(
        alignment: Alignment.centerRight,
        child: Text('رمز عبور',
            style: TextStyle(
                fontFamily: 'IranSans',
                color: Color.fromARGB(255, 166, 168, 195))));

    Widget _txtUserName = Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: AppDefaults.buttonBgColor.withOpacity(0.1),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ]),
      width: double.infinity,
      child: TextField(
        cursorColor: Colors.black,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            prefixIcon: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Image.asset(
                  'assets/images/Group 3800.png',
                  width: 40,
                )),
            suffixIcon: Padding(
                padding: EdgeInsets.only(right: 10),
                child: Image.asset(
                  'assets/images/Group 3799.png',
                  width: 40,
                )),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppDefaults.buttonBgColor,
                width: 1.5,
                style: BorderStyle.solid,
              ),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromARGB(255, 243, 243, 243),
                  width: 1.5,
                  style: BorderStyle.solid,
                ),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(10.0),
                )),
            contentPadding: EdgeInsets.only(left: 10),
            border: new OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 243, 243, 243),
                width: 1.5,
                style: BorderStyle.solid,
              ),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
            filled: true,
            fillColor: Colors.transparent),
      ),
    );

    Widget _txtPassword = Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ]),
      width: double.infinity,
      child: TextField(
        obscureText: true,
        cursorColor: Colors.black,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            suffixIcon: Padding(
                padding: EdgeInsets.only(right: 10),
                child: Image.asset(
                  'assets/images/Group 3801.png',
                  width: 40,
                )),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: AppDefaults.buttonBgColor,
                width: 1.5,
                style: BorderStyle.solid,
              ),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color.fromARGB(255, 243, 243, 243),
                  width: 1.5,
                  style: BorderStyle.solid,
                ),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(10.0),
                )),
            contentPadding: EdgeInsets.only(left: 10),
            border: new OutlineInputBorder(
              borderSide: BorderSide(
                color: Color.fromARGB(255, 243, 243, 243),
                width: 1.5,
                style: BorderStyle.solid,
              ),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10.0),
              ),
            ),
            filled: true,
            fillColor: Colors.transparent),
      ),
    );

    Widget _btnLogin = Container(
        height: 50,
        margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: FlatButton(
            padding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(color: Colors.transparent)),
            highlightColor: Colors.white,
            child: Text(
              'ورود',
              style: TextStyle(
                  color: Colors.white, fontFamily: 'IranSans', fontSize: 16),
            ),
            color: AppDefaults.buttonBgColor2,
            onPressed: () async {
              String query =
                  """loginInput:{emailOrPhoneNumber:"09364436650",password:"123123123",fcm:"RANDOM_STRING"} """;
            
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => DashboardPage()));
            }));

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/Mask Group 3.png'),
              fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        bottomNavigationBar: _btnLogin,
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.only(top: 100, left: 30, right: 30),
                child: Column(
                  children: [
                    _lblTitle,
                    SizedBox(height: 30),
                    Stack(
                        overflow: Overflow.visible,
                        children: [_avatar, _takePhoto]),
                    SizedBox(
                      height: 50,
                    ),
                    _lblUserName,
                    SizedBox(
                      height: 10,
                    ),
                    _txtUserName,
                    SizedBox(
                      height: 20,
                    ),
                    _lblPassword,
                    SizedBox(
                      height: 10,
                    ),
                    _txtPassword
                  ],
                ))),
      ),
    );
  }
}
