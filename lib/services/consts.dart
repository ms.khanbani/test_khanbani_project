import 'package:flutter/widgets.dart';

class AppDefaults {
  static const Color labelColor = Color.fromARGB(255, 61, 70, 114);
  static const Color buttonBgColor = Color.fromARGB(255, 3, 190, 205);
  static const Color buttonBgColor2 = Color.fromARGB(255, 127, 116, 199);
  static const Color focusBorderColor = Color.fromARGB(255, 3, 190, 205);
}
