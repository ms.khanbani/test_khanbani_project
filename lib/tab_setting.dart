import 'package:flutter/material.dart';
import 'package:test_project_khanbani/services/consts.dart';

class TabSetting extends StatefulWidget {
  @override
  _TabSettingState createState() => _TabSettingState();
}

class _TabSettingState extends State<TabSetting> {
  @override
  Widget build(BuildContext context) {
    Widget _bodyMessage = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(height: 20),
        Text(
          'سیستم موقعیت‌یاب شما غیر فعال است',
          style: TextStyle(
              fontFamily: 'IranSans',
              fontSize: 18,
              color: AppDefaults.labelColor),
        ),
        Text(
          'جهت مشاهده‌ی کسب وکارهای اطراف خود سیستم',
          style: TextStyle(
              fontFamily: 'IranSans',
              fontSize: 13,
              color: AppDefaults.labelColor),
        ),
        Text(
          'موقعیت‌یاب  را فعال کنید',
          style: TextStyle(
              fontFamily: 'IranSans',
              fontSize: 13,
              color: AppDefaults.labelColor),
        )
      ],
    );

    Widget _btnActiveGps = Container(
        width: 150,
        height: 46,
        child: FlatButton(
            padding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(color: Colors.transparent)),
            highlightColor: Colors.white,
            child: Text(
              'فعالسازی',
              style: TextStyle(
                  color: Colors.white, fontFamily: 'IranSans', fontSize: 14),
            ),
            color: AppDefaults.buttonBgColor2,
            onPressed: () {}));

    Widget _btnBarcode = Container(
        height: 37,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: AppDefaults.buttonBgColor.withOpacity(0.2),
                spreadRadius: 4,
                blurRadius: 5,
                offset: Offset(0, 1), // changes position of shadow
              )
            ],
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 5, 197, 214),
                Color.fromARGB(255, 0, 172, 179)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
            borderRadius: BorderRadius.circular(30.0)),
        child: FlatButton(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 0, top: 0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(color: Colors.transparent)),
            highlightColor: Colors.white,
            child: Row(
              children: [
                Text(
                  'پرداخت از طریق بارکد',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'IranSans',
                      fontSize: 11),
                ),
                Image.asset('assets/images/Group 2839.png', height: 17.6)
              ],
            ),
            color: Colors.transparent,
            onPressed: () {}));

    Widget _btnPayByAccount = Container(
        height: 37,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 98, 85, 150).withOpacity(0.2),
                spreadRadius: 4,
                blurRadius: 5,
                offset: Offset(0, 1), // changes position of shadow
              )
            ],
            gradient: LinearGradient(
              colors: [
                Color.fromARGB(255, 98, 85, 150),
                Color.fromARGB(255, 127, 116, 199)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
            borderRadius: BorderRadius.circular(30.0)),
        child: FlatButton(
            padding: EdgeInsets.only(left: 12, right: 12, bottom: 0, top: 0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
                side: BorderSide(color: Colors.transparent)),
            highlightColor: Colors.white,
            child: Row(
              children: [
                Text(
                  'پرداخت ازطریق نام کاربری',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'IranSans',
                      fontSize: 11),
                ),
                Image.asset('assets/images/email.png', height: 17.6)
              ],
            ),
            color: Colors.transparent,
            onPressed: () {}));

    Widget _header = Container(
      padding: EdgeInsets.only(top: 20, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ]),
      height: 97,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [_btnBarcode, _btnPayByAccount],
      ),
    );
    Widget _imgBg = Center(
        child: SizedBox(
      width: 252,
      height: 252,
      child: Image.asset('assets/images/Group 3802.png'),
    ));

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 247, 247, 249),
      body: Column(
        children: [
          _header,
          SizedBox(height: 20),
          _imgBg,
          _bodyMessage,
          SizedBox(height: 20),
          _btnActiveGps
        ],
      ),
    );
  }
}
